/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labpart1;

/**
 *
 * @author Admin
 */
public class Manager extends Employee {
       protected String name; 
    protected double salary;
    protected Double NoofHours;
    protected Double bonus;


    public Manager() {

    }

    public Manager(String name, double salary, double NoofHours, double bonus) {
        this.name = name;
        this.salary = salary;
        this.NoofHours = NoofHours;
        this.bonus = bonus;
    }
   public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getNoofHours() {
        return NoofHours;
    }

    public void setNoofHours(double NoofHours) {
        this.NoofHours = NoofHours;
    }
     public double getCalculatePay() {
    return NoofHours * salary + bonus;
  }

        @Override
    public String toString() {
        return "Manager Name: " + getName() +"\n"+ " Pay : " + getCalculatePay();
    }
    
}
